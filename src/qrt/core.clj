(ns qrt.core
  (:require [cljfx.api :as fx]
            [cljfx.css :as css])
  (:import [javafx.scene.canvas Canvas]
           [javafx.scene.paint Color])
  (:gen-class))


#_(defn- method-table
  "Tiny function to retreve method info of java classes.
  Only for development purpose in Repl-Driven-Development.
  https://stackoverflow.com/questions/5821286"
  [thing]
  (clojure.pprint/print-table (:members (clojure.reflect/reflect thing))))


#_(method-table org.opencv.objdetect.QRCodeDetector)


(def style
  (css/register ::style
                (let [padding 10
                      text-color "#DDEEFF"
                      background-color "#22222A"
                      text-bg "#333333"
                      border "#DDEEFF55"
                      border-radious 5]
                  {::padding padding
                   ::text-color text-color
                   ::background-color background-color
                   ".root" {:-fx-padding padding
                            :-fx-background-color background-color}
                   ".label" {:-fx-text-fill text-color
                             :-fx-wrap-text true
                             :-fx-padding padding}
                   ".text-field" {:-fx-background-color text-bg
                                  :-fx-text-fill text-color
                                  :-fx-border-color border
                                  :-fx-border-radius border-radious}
                   ".context-menu" {:-fx-background-color background-color}})))


(def *state (atom {:input ""}))


(defn qr-input [{:keys [input]}]
  {:fx/type :text-field
   :on-text-changed #(swap! *state assoc :input %)
   :text input})


(defn putpixel [canvas x y color]
  (doto canvas
    (.setFill (Color/gray color))
    (.fillRect x y 1 1)))


(defn canvas-xor [{:keys [width height]}]
  {:fx/type :canvas
   :width width
   :height height
   :draw (fn [^Canvas canvas]
           (let [c (.getGraphicsContext2D canvas)]
             (.setFill c (Color/gray 0.5))
             (.fillRect c 0 0 width height)
             (dorun
              (for [x (range width)
                    y (range height)]
                (do
                  (putpixel c x y (/ (bit-xor x y) 255.0)))))))})


(defn root [{:keys [input progress]}]
  {:fx/type :stage
   :showing true
   :title "QR studio"
   :scene {:fx/type :scene
           :stylesheets [(::css/url style)]
           :root {:fx/type :v-box
                  :alignment :center
                  :children [{:fx/type :h-box
                              :alignment :center
                              :children [{:fx/type :label
                                          :text "Input:"}
                                         {:fx/type qr-input
                                          :title input}]}
                             {:fx/type canvas-xor
                              :width 256
                              :height 256}]}}})


(def renderer
  (fx/create-renderer
   :middleware (fx/wrap-map-desc assoc :fx/type root)))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (fx/mount-renderer *state renderer))
