(defproject qrt "0.1.0-SNAPSHOT"
  :description "QRt"
  :url "https://devheroes.codes/pouya-abbassi/qrt"
  :license {:name "MIT License"
            :url "https://choosealicense.com/licenses/mit/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [cljfx "1.7.13"]
                 [cljfx/css "1.0.0"]
                 [opencv/opencv "4.5.1"]
                 [opencv/opencv-native "4.5.1"]
                 [com.google.zxing/core "3.4.1"]
                 [com.google.zxing/javase "3.4.1"]]
  :main ^:skip-aot qrt.core
  :target-path "target/%s"
  :profiles {:dev {:plugins [[lein-shell "0.5.0"]]
                   :jvm-opts ["-Dcljfx.skip-javafx-initialization=false"]}
             :uberjar {:aot :all
                       :jvm-opts ["-Dcljfx.skip-javafx-initialization=true"]}}
  :jar-exclusions [#"^docs/.*" #"^resources/.*"]
  :aliases
  {"native"
   ["shell"
    "native-image" "--report-unsupported-elements-at-runtime"
    "--initialize-at-build-time"
    "-jar" "./target/uberjar/${:uberjar-name:-${:name}-${:version}-standalone.jar}"
    "-H:Name=./target/${:name}"]})
